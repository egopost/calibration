% ROI dialog function
function [output] = dial(Dial, prompt, title);

if (Dial == 0)
    defaultans = {'1', 'A12:CS76'}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 50; 1 50], defaultans, options);
elseif (Dial == 1)     
    options.Interpreter = 'tex';
    options.Default = 'No';
    out = questdlg(prompt,title,'Yes','No',options);
    if (string(out) == 'Yes')
        output = 'Y';
    else
        output = 'N';
    end
elseif (Dial == 2)
    defaultans = {'125', '0', '12'}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 50; 1 50; 1 50], defaultans, options);
elseif (Dial == 3)
    defaultans = {'1e9', '-2.35', '0', '1e6', '5e9', '-3', '-2.3', '0', '0'}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 50; 1 50; 1 50;1 50; 1 50; 1 50; ...
                                      1 50; 1 50; 1 50], defaultans, options);                                
elseif (Dial == 4)     
    options.Interpreter = 'tex';
    options.Default = 'Calibration curves';    
    output = questdlg(prompt,title,'Plate uniformity','Calibration curves','Time-kill assay',options);
elseif (Dial == 5)    
    defaultans = {'1', 'A', '0 2 4 6 24', 'Compaund 1'}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 50; 1 50; 1 50; 1 50], defaultans, options);
elseif (Dial == 6)    
    defaultans = {'5e7', '5e6', '5e5', '5e4', '5e3', '5e2', '5e1', '0'}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50; 1 50], defaultans, options);
elseif (Dial == 7)    
    defaultans = {''}; 
    options.Resize='on';
    options.WindowStyle='normal';
    options.Interpreter='tex';
    output = inputdlg(prompt, title, [1 80], defaultans, options);
end

