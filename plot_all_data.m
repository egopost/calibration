% plot data, time range and intersection curves output
function[i_ini, i_end, time_k, curve0, curve] = plot_all_data(data, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

time = squeeze(data(:,1));

i_ini = 2;
i_end = size(data, 2); 

%legend_name = ['1e8'; '1e7'; '1e6'; '1e5'; '1e4'; '1e3'; '1e2'; '1e1'];
Y_level = 180;
time_min = 0;
time_max = 10;
        
    str_Y = 'Y';    
    while str_Y == 'Y'   
    
        curve0 = ones(1,length(time)).*Y_level;
        
        fig = figure;
        for i = i_ini:i_end  
            curve(:,i) = data(:,i);
            plot(time,curve(:,i),'.-')
            hold on
            title_name = ['96 well plate all kinetic curves'];
            title(title_name)
            xlabel('t, h')
            ylabel('Intensity')
        end
        %legend(legend_name, 'AutoUpdate', 'off');
        plot(time, curve0, 'k.-')
        xline(time_min,'r--');
        xline(time_max,'r--');
     
        Dial = 1;
        prompt = {'\fontsize{14} Would you like to renew threshold Y/N?'};
        Title = 'Threshold'; 
        str_Y = dial(Dial,prompt, Title);
        
        if (str_Y == 'Y')         
            Dial = 2;
            prompt = {'\fontsize{14} Y level', '\fontsize{14} t min', '\fontsize{14} t max'};
            Title = 'Input threshold and time range';
            output = dial(Dial,prompt,Title);
            Y_level = str2num(output{1});
            time_min = str2num(output{2});
            time_max = str2num(output{3});
            close(fig);
        end
    
    end
    
print_file_name = ['96_well_plate_all_kinetic_curves_', g_str_file_name];
print(fig, print_file_name, '-djpeg')
    
time_k = find(time>=time_min & time<=time_max);

hold off
