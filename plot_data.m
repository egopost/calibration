% plot data, time range and intersection curves output
function[time_k, curve0, curve, curve1, curve2, curve3] = plot_data(k, data, CFU_str, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

time = squeeze(data(:,1));

N_col = 12; % number of collumn in plate
N_row = 8; % number of rows in plate
N_xplet = 3; % number collumb in x-plet

i_ini = 2; % first cow contain time data
i_end = (N_row-1)*N_col+2; 

legend_name = CFU_str;
%legend_name = ['5e8'; '5e7'; '5e6'; '5e5'; '5e4'; '5e3'; '5e2'; '5e1'];
Threshold = 125;
time_min = 0;
time_max = 12;
        
str_Y = 'Y';    
while str_Y == 'Y'   
    
    curve0(:,1) = ones(1,length(time)).*Threshold;
        
    j = 1; %Number of curve
    fig(k) = figure;
    for i = i_ini+(k-1)*N_xplet:N_col:i_end+(k-1)*N_xplet
        
        %curve(:,j)       = mean(data(:,i:i+2),2);
        curve_left(:,j)  = data(:,i);
        curve_cent(:,j)  = data(:,i+1);
        curve_right(:,j) = data(:,i+2);
        
        % calculation curve(ave) in the case if some curves is zero
        curve_ave = zeros(size(data, 1), 1);
        index_ave = 0;
        for ii = 0:2
            curve_ave = curve_ave + data(:,i+ii);
            if (sum(data(:,i+ii)) ~= 0)
                index_ave = index_ave + 1;
            end
        end
            
        if (index_ave == 0)
            curve(:,j) = curve_ave;
        else
            curve(:,j) = curve_ave./index_ave;
        end
        %------------------------------------------------------------------
        
        plot(time,curve(:,j),'.-')
        hold on
        title_name = ['Data calibration curve', num2str(k)];
        title(title_name)
        xlabel('time, hours')
        ylabel('Omnilog units')
        j = j+1;
    end

    legend(legend_name, 'AutoUpdate', 'off');
    plot(time, curve0, 'k.-')
    xline(time_min,'r--');
    xline(time_max,'r--');
     
    Dial = 1;
    prompt = {'\fontsize{14} Would you like to renew threshold/time limit Y/N?'};
    Title = 'Threshold'; 
    str_Y = dial(Dial,prompt, Title);
        
    if (str_Y == 'Y')
        Dial = 2;
        prompt = {'\fontsize{14} Threshold', '\fontsize{14} t min', '\fontsize{14} t max'};
        Title = 'Input Y level and time range';
        output = dial(Dial,prompt,Title);
        Threshold = str2num(output{1});
        time_min = str2num(output{2});
        time_max = str2num(output{3});
            
        close(fig(k));
     end   
end
        
print_file_name = ['data_calibration_curve_', num2str(k)];
print(fig(k), print_file_name, '-djpeg')
    
curve1 = curve_left;
curve2 = curve_cent;
curve3 = curve_right;

time_k(:,1) = find(time>=time_min & time<=time_max);

