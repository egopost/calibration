% Intersection
function intersecton_all(i_ini, i_end, time, time_k, curve0, curve, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

% Y level curve
L0 = [time(time_k(:,1))'; curve0(time_k(:,1))];
    
for i = i_ini:i_end
    L = [time(time_k)'; curve(time_k,i)'];
    Inters_point = Interx(L,L0);
    if (isempty(Inters_point))
        Inters_points(:,i) = [NaN; i];
    else
        Inters_points(:,i) = [Inters_point(1,1); i];
    end
end

% Set cell names ----------------------------------------------------------
N_length = 12; % lenght
N_width  = 8; % lenght
liter = {'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'};

for i = 1:N_width,
    for j = 1:N_length,
    k = (i-1)*12 + j;   
    str_cells{k} = [liter{i}, num2str(j)];
    end
end

%writing fitting curve data to file
file_name = ['all_data_interx_', g_str_file_name, '.xlsx'];
header1={'Intersection data for threshold '};
header2=num2cell(curve0(1,1));
sheet = 1;
xlRange = 'A1';
xlswrite(file_name,header1,sheet,xlRange)
sheet = 1;
xlRange = 'E1';
xlswrite(file_name,header2,sheet,xlRange)

Time = Inters_points(1,2:size(Inters_points,2))';
Well = str_cells';
T = table(Time, Well);

writetable(T,file_name,'Sheet',1,'Range','A2')

fprintf(logID,'All data intersection points have been written to file: %s\n', file_name);
msgbox('Data file has been created','Success');

