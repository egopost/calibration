% plot data, time range and intersection curves output
function[fig] = plot_all_data_only(data, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;
color = ['g'; 'g'; 'g'; 'r'; 'r'; 'r'; 'b'; 'b'; 'b'; 'k'; 'k'; 'k'];

time = squeeze(data(:,1));

N_col = 12; % number of collumn in plate
N_row = 8; % number of rows in plate
i_ini = 2; % first cow contain time data 

fig = figure;
for i = 1:N_row
    for j = i_ini:N_col+1
        k = (i-1)*N_col + j;
        h(j) = plot(time,data(:,k),'.-','Color',color(j-1));
        hold on
        title_name = '96 well plate all kinetic curves';
        title(title_name)
        xlabel('t, h')
        ylabel('Intensity')
    end
end

legend_name = {'Calibration curves'; '4-5-6 curves'; '7-8-9 curves'; '10-11-12 curves'};
legend(h([2 5 8 11]), legend_name);
    
print_file_name = ['96_well_plate_all_kinetic_curves_', g_str_file_name];
print(fig, print_file_name, '-djpeg')


