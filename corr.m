
% Data curve calibration  Ver 1.02.
% -------------------------------------------------------------------------   
% Ilina Polina, Sergey Dyadechkin
% -------------------------------------------------------------------------
% Requires following m.files
% 1.  open_data_file.m
% 2.  dial.m
% 3.  plot_data.m
% 4.  intersection.ml
% 5.  plot_all_data.m
% 6.  intersection_all.m
% 7.  fitting.m
% 8.  Interx.m
% 9.  data_calibration.m
% 10. time_kill_curve.m     
% 11. plot_all_data_only.m         

clear all;

% Choose new workspace folder to there all results are saved
title = 'Choose workspace folder';
current_path = pwd;
new_path = uigetdir(current_path,title);
cd(new_path);
addpath(current_path);

global g_str_file_name;
% Dialog: Add string to file name
str_file_name = inputdlg({'Add string to file name'},...
              'String to file name', [1 70]); 
g_str_file_name =  str_file_name{1};

% Create log file. Writing all information about script running
logID_name = ['Data_calibration_', g_str_file_name, '.log'];
logID = fopen(logID_name, 'wt');
fprintf(logID, ['File: ', logID_name, '\n']);
time = datestr(clock,'YYYY/mm/dd HH:MM:SS:FFF');
fprintf(logID,'Logging time: %s\n',time);

Dial = 4;
prompt = {'\fontsize{14} Choose futher programe mode'};
Title = 'Programe mode'; 
answer = dial(Dial,prompt, Title);

% Insert CFU/ml
Dial = 6;
prompt = {'\fontsize{12} A', '\fontsize{12} B', '\fontsize{12} C', '\fontsize{12} D', ...
          '\fontsize{12} E', '\fontsize{12} F', '\fontsize{12} G', '\fontsize{12} H'};
        Title = 'CFU/ml, A->H highest first';
        output = dial(Dial,prompt,Title);
        for i = 1:8
            out(i) = str2num(output{i});
            CFU(i) = out(i);
            CFU_str{i} = output{i};
        end
        
% Extraction data from data files
[data] = open_data_file(logID);

% time 
time = squeeze(data(:,1));

switch answer
    case 'Plate uniformity'
        % plot and curves and time range data output
        [i_ini, i_end, time_k, curve0, curve] = plot_all_data(data, logID);

        % return intersection points of curve0 and all data
        intersecton_all(i_ini, i_end, time ,time_k, curve0, curve, logID);

    case 'Calibration curves'    
        %N_calibration = 3; % Number fo calibration curves
        
        Dial = 7;
        prompt = {'\fontsize{12} Insert number of curves to plot (0-4)'};
        Title = 'Insertion curves number';
        output = dial(Dial,prompt,Title);
        N_calibration = str2num(output{1});
    
        for k = 1:N_calibration
            % plot and curves and time range data output
            [time_k, curve0, curve, curve1, curve2, curve3] = plot_data(k, data, CFU_str, logID);

            % return intersection points of curve0 and CFU
            [ip, std_time] = intersecton(k, time ,time_k, curve0, curve, ...
                                                    curve1, curve2, curve3, CFU, logID);
               
            ip_non_zero = find(ip(2,:) ~= 0);
            if(length(ip_non_zero) >= 2)
                % fitting calibration curves
                [a, b, c, fit_cfu] = fitting(k, time, time_k, ip, std_time, logID);
            end
        end
    case 'Time-kill assay'
        % plot all curves exept the calibration curves
        fig = plot_all_data_only(data, logID);
       
        dil = warndlg('Press any key to proceed', 'Picture time');
        waitfor(dil);
        close(fig);
        
        % in this case only one calibration curve
        k = 1; 
        
        % plot and curves and time range data output
        [time_k, curve0, curve, curve1, curve2, curve3] = plot_data(k, data, CFU_str, logID);

        % return intersection points of curve0 and CFU
        [ip, std_time] = intersecton(k, time ,time_k, curve0, curve, ...
                                                    curve1, curve2, curve3, CFU, logID);
        % fitting calibration curves
        [a, b, c, fit_cfu] = fitting(k, time, time_k, ip, std_time, logID);
                
        % calibration curve usage
        [cfu_ave, cfu_std, file_name] = data_calibrate(a, b, c, time, time_k, curve0, data, logID);
        
        dil = warndlg('Data file has been created', 'Success');
        waitfor(dil);
        
        Dial = 1;
        prompt = {'\fontsize{14} Would you like to build time-kill curves Y/N?'};
        Title = 'Time-kill curve'; 
        str_Y = dial(Dial,prompt, Title);
        
        if (str_Y == 'Y')
            %close(fig1);
            time_kill_curve(cfu_ave, cfu_std, file_name, logID);
        end
end

% close log file
fclose(logID);

% return to current path
cd(current_path);   






