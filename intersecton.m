% Intersection
function [ip, std_time] = intersecton(k, time, time_k, curve0, curve, ... 
                                                     curve1, curve2, curve3, CFU, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

curve_left  = curve1;
curve_cent  = curve2;
curve_right = curve3;
    
L0 = [time(time_k)'; curve0(time_k)'];
    
for i = 1:8
    L = [time(time_k)'; curve(time_k,i)'];
    L_left  = [time(time_k)'; curve_left(time_k,i)'];
    L_cent  = [time(time_k)'; curve_cent(time_k,i)'];
    L_right = [time(time_k)'; curve_right(time_k,i)'];
    Ip = Interx(L,L0);
    Ip_left  = Interx(L_left,L0);
    Ip_cent  = Interx(L_cent,L0);
    Ip_right = Interx(L_right,L0);
    
    if (isempty(Ip)) 
        Ips(:,i) = [0; 0];
    else
        Ips(:,i) = [Ip(1,1); CFU(i)];
    end
       
    if (isempty(Ip_left)) 
        Ips_left(:,i) = [0; 0];
    else
        Ips_left(:,i) = [Ip_left(1,1); CFU(i)];
    end
    
    if (isempty(Ip_cent)) 
        Ips_cent(:,i) = [0; 0];
    else
        Ips_cent(:,i) = [Ip_cent(1,1); CFU(i)];
    end
    
    if (isempty(Ip_right)) 
        Ips_right(:,i) = [0; 0];
    else
        Ips_right(:,i) = [Ip_right(1,1); CFU(i)];
    end
end

time_data = [Ips_left(1,:); Ips_cent(1,:); Ips_right(1,:)];

for i = 1:8
    kk = find(time_data(:,i) ~= 0);
    time_data_new = time_data(kk,i);
    std_time(i)  = std(time_data_new,0,1);
end
    
ip  = Ips;

%writing fitting curve data to file
file_name = ['calibration_data_curve_',num2str(k), g_str_file_name, '.xlsx'];
header1={'Intersection data for threshold '};
header2=num2cell(curve0(1,1));
sheet = 1;
xlRange = 'A1';
xlswrite(file_name,header1,sheet,xlRange)
sheet = 1;
xlRange = 'E1';
xlswrite(file_name,header2,sheet,xlRange)

Time   = Ips(1,1:size(Ips,2))';
Time_1 = Ips_left(1,1:size(Ips_left,2))';
Time_2 = Ips_cent(1,1:size(Ips_cent,2))';
Time_3 = Ips_right(1,1:size(Ips_right,2))';
Time_std = std_time(1,1:size(std_time,2))';
CFU = Ips(2,1:size(Ips,2))';
    
T = table(Time, Time_1, Time_2, Time_3, Time_std, CFU);
writetable(T,file_name,'Sheet',1,'Range','A2')
    
%fclose(fid);
fprintf(logID,'\nThreshold = %f\n\n', curve0(1,1));

fprintf(logID,'Calibration curve data have been written to data file: %s\n', file_name);

