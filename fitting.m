% Fitting intersection curves
function [a, b, c, fit_cfu] = fitting(k, time, time_k, ip, std_time, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

% remove zero points from intersection points data
k0 = find(ip(1,:) == 0);
ip(:,k0) = [];
std_time(k0) = [];

startPoint = [1e9 -2.35 0];
lb = [1e6, -3, 0.00];   
ub = [5e9, -2.3, 0.00];
    
str_Y = 'Y';    
while str_Y == 'Y'
      t   = ip(1,:);
      cfu = ip(2,:);

      single_exp = @(p,time) p(1)*exp(p(2)*time) + p(3);
      opts = optimset('Display','off');
    
      [coeff,~,res,exitflag,~,~,J] = lsqcurvefit(single_exp,startPoint,t,cfu,lb,ub,opts);

      a(k) = coeff(1);
      b(k) = coeff(2);
      c(k) = coeff(3);

      fit_cfu(:,k) = a(k)*exp(b(k)*time) + c(k);
        
      fig1(k)=figure;
      semilogy(ip(1,:),ip(2,:),'bo-')
      hold on
      semilogy(time(time_k),fit_cfu(time_k,k),'r.-')
      grid on
      title_name = ['Calibrated curve fitting for bunch curves ', num2str(k)];
      title(title_name)
      xlabel('t, h')
      ylabel('CFU')
      legend('Calibration data', 'Calibration fitting', 'AutoUpdate', 'off')
      errorbar(ip(1,:),ip(2,:),std_time,'horizontal','b')
        
      Dial = 1;
      prompt = {'\fontsize{14} Would you like to renew fitting Y/N?'};
      Title = 'Fitting'; 
      str_Y = dial(Dial,prompt, Title);
        
      if (str_Y == 'Y')
          Dial = 3;
          prompt = {'\fontsize{14} Start point a', ...
                    '\fontsize{14} Start point b', ...
                    '\fontsize{14} Start point c', ...
                    '\fontsize{14} min(a)', ...
                    '\fontsize{14} max(a)', ...
                    '\fontsize{14} min(b)', ...
                    '\fontsize{14} max(b)', ...
                    '\fontsize{14} min(c)', ...
                    '\fontsize{14} max(c)', ...
                    };
          Title = 'Fitting method a*exp(b*t)+c';
          output = dial(Dial,prompt,Title);
          startPoint = [str2num(output{1}) str2num(output{2}) str2num(output{3})];
          lb = [str2num(output{4}), str2num(output{6}), str2num(output{8})];   
          ub = [str2num(output{5}), str2num(output{7}), str2num(output{9})];
            
          close(fig1(k));
      end   
end

print_file_name = ['calibrated_curve_fitting_', g_str_file_name, num2str(k)];
print(fig1(k), print_file_name, '-djpeg');

fprintf(logID, 'Fitting parametes \n');
fprintf(logID, 'a\t\t b\t\t c \n');
fprintf(logID, '%E\t %E\t %E \n\n', a(k), b(k), c(k));
fprintf(logID, 'Start point a\t Start point b\t Start point c \n');
fprintf(logID, '%E\t %E\t %E \n\n', startPoint(1), startPoint(2), startPoint(3));
fprintf(logID, 'min(a)\t\t min(b)\t\t min(c)\n');
fprintf(logID, '%E\t %E\t %E \n\n', lb(1), lb(2), lb(3));
fprintf(logID, 'max(a)\t\t max(b)\t\t max(c)\n');
fprintf(logID, '%E\t %E\t %E \n\n', ub(1), ub(2), ub(3));

