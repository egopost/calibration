function time_kill_curve(cfu_ave, cfu_std, file_name, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

% replace 0 element in cfu_ave to 1
cfu_ave(find(cfu_ave == 0)) = 1;

N_row = 8; % number of rows in plate 
liter = ['A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'];
color = ['r'; 'b'; 'k'; 'g'; 'c'; 'y'; 'm'; 'r'];

% time points definition
time_exposure = ones(24,1).*NaN;

ci = 0; % Curve number

legend_name = [];
fig2 = figure;

str_Y = 'Y';    
    while str_Y == 'Y'
        
        Dial = 5;
        prompt = {'\fontsize{12} Column number (1-3)', ...
                  '\fontsize{12} Starting row number (A-H)', ...
                  '\fontsize{12} Time points e.g. (0 2 4 6)', ...
                  '\fontsize{12} Curve legend e.g. (Compaund 1)'};
        Title = 'Time-kill curve parameters';
        output = dial(Dial,prompt,Title);
        col = str2num(output{1});
        row = output{2};
        time_points = str2num(output{3});
        line_name = output{4};
        
        row_pos = find(liter == row);
        
        if (length(liter) < row_pos-1 + length(time_points))
            dil = warndlg('Row start point and time point number is not consistant (>8)', 'Warning');
            waitfor(dil);
            continue;
        end
        ci = ci + 1;
        data_ini = N_row*(col-1) + row_pos;
        data_end = data_ini + length(time_points) - 1;
        cfu_ave_data = cfu_ave(data_ini:data_end);
        cfu_std_data = cfu_std(data_ini:data_end);
        
        % Time points which will be saved in xls file
        time_exposure(data_ini:data_end) = time_points; 
        
        h(ci) = semilogy(time_points,cfu_ave_data,'-o','Color',color(ci));
        h(ci).LineWidth = 1.5;
        hold on
        grid on
        title('Time-kill curves')
        xlabel('t,h')
        ylabel('CFU/ml')
        legend_name = [legend_name; {line_name}];
        legend(h([1:ci]), legend_name);
        legend('AutoUpdate', 'off');
        er(ci) = errorbar(time_points,cfu_ave_data,cfu_std_data,'vertical','.','Color',color(ci));
        
        Dial = 1;
        prompt = {'\fontsize{14} Would you like to add another curve Y/N?'};
        Title = 'Add curve'; 
        str_Y = dial(Dial,prompt, Title);
    end
    
print_file_name = ['time_kill_curves_', g_str_file_name];
print(fig2, print_file_name, '-djpeg')

T = table(time_exposure);
writetable(T,file_name,'Sheet',1,'Range','K2')

    