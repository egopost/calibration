% Open data file
function [data] = open_data_file(logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

filename = "string";
    
[filename, path]=uigetfile('*.xlsx');
if isequal(filename,0)
   disp('Selected file: Cancel');
else
str = sprintf('Selected file: %s %s', fullfile(path, filename)); 
disp(str);
fprintf(logID,'Selected file: %s: %s\n', fullfile(path, filename));
end

Dial = 0;
prompt = {'\fontsize{14} Excel sheet number', '\fontsize{14} Data range'};
Title = 'Input sheet number and data range choose';
output = dial(Dial,prompt,Title);
sheet = str2num(output{1});
xlRange = output{2};

data = xlsread(filename,sheet,xlRange);

% clearing data -----------------------------------------------------------
N_length = 12; % lenght
N_width  = 8; % lenght
liter = {'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'};

for i = 1:N_width,
    for j = 1:N_length,
    k = (i-1)*12 + j;   
    str_cells{k} = [liter{i}, num2str(j)];
    end
end

Dial = 7;
prompt = {'\fontsize{12} Data columns to remove from analysis e.g. (A4 A5 A6)'};
        Title = 'Cleaning data selection';
        output = dial(Dial,prompt,Title);
        str_data_name = strsplit(output{1});        

% Renew data: set data column to for k_name column
k_name = find(ismember(str_cells, str_data_name));

data(:,k_name+1) = 0;
             
disp(['Loaded data from file. Martix dimensions: ', ...
      num2str(size(data, 1)), ',', num2str(size(data, 2))])
fprintf(logID,'Created 2D data matrix. Martix dimensions: %d %d\n', ...
        size(data, 1), size(data, 2));

end
