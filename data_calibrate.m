% plot data, time range and intersection curves output
function[cfu_ave, std_cfu, file_name] = data_calibrate(a, b, c, time, time_k, curve0, data, logID);
% Call and log function name
fn = dbstack;
name_fn = fn.name;
fprintf(logID,'\nCall function: %s\n', name_fn);

global g_str_file_name;

N_col = 12; % number of collumn in plate
N_row = 8; % number of rows in plate
N_xplet = 3; % number collumb in x-plet

i_ini = 5; % first cow contain time data
liter = {'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'};

% definition of cell names for output data file
str_cell = {};
for j = 4:N_xplet:N_col
    for i = 1:N_row
        str_cells = [liter{i}, num2str(j), num2str(j+1), num2str(j+2)];
        str_cell{end + 1} = str_cells;
    end
end

L0 = [time(time_k)'; curve0(time_k)'];

i_count = 0;
for j = i_ini:N_xplet:N_col
    for i = 1:N_row
        i_count = i_count + 1;
        k = (i-1)*N_col + j;
        %curve = mean(data(:,k:k+2),2);
        curve_left  = data(:,k);
        curve_cent  = data(:,k+1);
        curve_right = data(:,k+2);
        
        % calculation curve(ave) in the case if some curves is zero
        curve_ave = zeros(size(data, 1), 1);
        index_ave = 0;
        for kk = 0:2
            curve_ave = curve_ave + data(:,k+kk);
            if (sum(data(:,k+kk)) ~= 0)
                index_ave = index_ave + 1;
            end
        end
            
        if (index_ave == 0)
            curve = curve_ave;
        else
            curve = curve_ave./index_ave;
        end
        %------------------------------------------------------------------
                
        L       = [time(time_k)'; curve(time_k)'];
        L_left  = [time(time_k)'; curve_left(time_k)'];
        L_cent  = [time(time_k)'; curve_cent(time_k)'];
        L_right = [time(time_k)'; curve_right(time_k)'];
        
        Ip = Interx(L,L0);
        Ip_left  = Interx(L_left,L0);
        Ip_cent  = Interx(L_cent,L0);
        Ip_right = Interx(L_right,L0);
        
        % average point
        if (isempty(Ip))
            Ips(i_count) = 0;
        else
            Ips(i_count) = Ip(1,1);
        end
        
        % left point
        if (isempty(Ip_left))
            Ips_left(i_count) = 0;
        else
            Ips_left(i_count) = Ip_left(1,1);
        end
        
        % central point
        if (isempty(Ip_cent))
            Ips_cent(i_count) = 0;
        else
            Ips_cent(i_count) = Ip_cent(1,1);
        end
        
        % right point
        if (isempty(Ip_right))
            Ips_right(i_count) = 0;
        else
            Ips_right(i_count) = Ip_right(1,1);
        end
    end
end

% dilution coeff
dil_coeff = 100;

% calculation cfu accroding to interpolation curve
for i = 1:i_count,
    if (Ips(i) == 0) 
        cfu_ave(i) = 0; 
    else
        cfu_ave(i) = a*exp(b*Ips(i)) + c;
    end
    
    if (Ips_left(i) == 0)
        cfu_left(i) = 0;
    else
        cfu_left(i) = a*exp(b*Ips_left(i)) + c;
    end
    
    if (Ips_cent(i) == 0)
        cfu_cent(i) = 0;
    else
        cfu_cent(i) = a*exp(b*Ips_cent(i)) + c;
    end
    
    if (Ips_right(i) == 0)
        cfu_right(i) = 0;
    else
        cfu_right(i) = a*exp(b*Ips_right(i)) + c;
    end
    
    cfu_ave(i) = cfu_ave(i).*dil_coeff;
    cfu_left(i) = cfu_left(i).*dil_coeff;
    cfu_cent(i) = cfu_cent(i).*dil_coeff;
    cfu_right(i) = cfu_right(i).*dil_coeff;
    
    cfu_data = [cfu_left(i) cfu_cent(i) cfu_right(i)];   
    
    kk = find(cfu_data ~= 0);
    std_cfu(i)  = std(cfu_data(kk));
end

%writing fitting curve data to file
file_name = ['data_time_kill_', g_str_file_name, '.xlsx'];
header1={'Intersection CFU data for threshold '};
header2=num2cell(curve0(1,1));
sheet = 1;
xlRange = 'A1';
xlswrite(file_name,header1,sheet,xlRange)
sheet = 1;
xlRange = 'E1';
xlswrite(file_name,header2,sheet,xlRange)

format short e;
Curve_name = str_cell';
Time_ave   = Ips';
Time_left  = Ips_left';
Time_cent  = Ips_cent';
Time_right = Ips_right';
CFU_ave    = cfu_ave';
CFU_1      = cfu_left';
CFU_2      = cfu_cent';
CFU_3      = cfu_right';
CFU_std    = std_cfu';
    
T = table(Curve_name, Time_ave, Time_left, Time_cent, Time_right, CFU_ave, CFU_1, CFU_2, CFU_3, CFU_std);
writetable(T,file_name,'Sheet',1,'Range','A2')

